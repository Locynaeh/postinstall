# Post-install Instruction (this is not script) to set-up my basic usally-used Debian Librazik2

# Add autocomplétion system-wide
sudo apt install bash-completion
sudo nano /etc/bash.bashrc

# Add this at the end of the file to use auto-completion:
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# If stable version of Mozilla Firefox is prefered to the ESR 
# Download the choosen version of Mozilla Firefox on the official website (stable, dev...) then do:
sudo tar xjvf Téléchargements/firefox-65.0.tar.bz2 -C /opt/ # According to the version downloaded
sudo ln -s /opt/firefox/firefox /usr/local/bin/
sudo nano /usr/share/applications/firefox.desktop

# Add this in the .desktop to be created:
[Desktop Entry]
Name=Firefox
Comment=Navigue sur le web
GenericName=Navigateur Web
X-GNOME-FullName=Navigateur Web Firefox
Exec=/opt/firefox/firefox %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=/opt/firefox/browser/chrome/icons/default/default128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xm$
StartupWMClass=Firefox
StartupNotify=true

# If necessary according to the language
sudo apt install firefox-l10n-fr

# Diverses applications, utilities, dev et multimedias
sudo apt install gdebi clementine hexchat pinta krita gimp mypaint inkscape kolourpaint4 git retext kile freeplane geany geany-common geany geany-plugins geany-plugin-markdown geany-plugins-common jedit vlc gajim

# To install Librazik2, please follow instructions on the official website (https://librazik.tuxfamily.org/) to add the repository and:
sudo apt install librazik-optimisations-all librazik-banquesdeson-all librazik-base-logicielsaudio librazik-base-systemeaudiomidi librazik-mate-menu
 
# Installation of others audio software, non-included in the meta-packets of LBZ2
sudo apt install ams vmpk supercollider puredata rakarrack





